﻿// лаб8.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;
#include <fstream>
#define N 10000
#define M 1000
#define K 100
#define L 100


int main()
{
	setlocale(LC_ALL, "Rus");
	int n;
	cout << "введите количество чисел: " << endl;
	cin >> n;

	int* mas = new int[n];
	int* last = new int[n];
	int* sum = new int[n];



	for (int i = 0; i < n; i++)
	{
		cin >> mas[i];

		last[i] = mas[i] % 10; 

		sum[i] = 0;
		int x = mas[i];
		while (x > 0)
		{
			sum[i] += x % 10;
			x /= 10;
		}
	}
	for (int i = 0; i < n - 1; i++)
		for (int j = i + 1; j < n; j++)
		{
			if ((last[i] > last[j]) ||
				(last[i] == last[j] && sum[i] > sum[j]) ||
				(last[i] == last[j] && sum[i] == sum[j] && mas[i] > mas[j]))
			{
				swap(mas[i], mas[j]);
				swap(last[i], last[j]);
				swap(sum[i], sum[j]);
			}
		}
	for (int i = 0; i < n; i++)
	{
		cout << mas[i] << " " << endl;
	}
	delete[] mas;
	delete[] last;
	delete[] sum;
	return 0;
}


/*int main()
{
	int p, q;
	cin >> p;
	cin >> q;
	int** matrix = new int*[p];
	for (int i = 0; i < p; i++)
	{
		matrix[i] = new int[q];
	}
	int n, m;
	int minn = INT_MAX;
	int n_s;
	setlocale(LC_ALL, "Rus");

	ifstream fin("fin.txt");
	ofstream fout("out123.txt");

	//fout << "введите размер матрицы: " << endl;
	fin >> n >> m;
	//fout << "введите значения матрицы: " << endl;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			fin >> matrix[i][j];
		}
	}
	//fout << "ваша матрица: " << endl;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			fout << matrix[i][j] << "\t";
		}
	}   fout << endl;
	fout << endl;

	for (int j = 0; j < m; j++)
	{
		for (int i = 0; i < n; i++)
		{
			if (matrix[i][j] < minn)
			{
				minn = matrix[i][j];
				n_s = j;
			}
		}
	}
	fout << endl;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{


			if (matrix[i][n_s] < 0)
			{
				matrix[i][n_s] = 0;
			}
		}
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			fout << matrix[i][j] << "\t";
		}
		fout << endl;
	}

	fin.close();
	fout.close();
	for (int i = 0; i < p; i++)
	{
		delete[] matrix[i];
	}
	delete[] matrix;

	return 0;
}
*/